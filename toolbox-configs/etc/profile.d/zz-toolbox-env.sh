generators="/usr/lib/systemd/user-environment-generators/"
if [ -f /run/.containerenv ] && [ -d $generators ]; then
    for gen in ${generators}*; do
        eval "`$gen | sed 's/^/export /'`"
    done
fi
