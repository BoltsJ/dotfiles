scriptencoding utf-8
" XDG support

if empty($MYVIMRC) | let $MYVIMRC = expand('<sfile>:p') | endif

if empty($XDG_CACHE_HOME)  | let $XDG_CACHE_HOME  = $HOME."/.cache"       | endif
if empty($XDG_CONFIG_HOME) | let $XDG_CONFIG_HOME = $HOME."/.config"      | endif
if empty($XDG_DATA_HOME)   | let $XDG_DATA_HOME   = $HOME."/.local/share" | endif
if empty($XDG_STATE_HOME)  | let $XDG_STATE_HOME  = $HOME."/.local/state" | endif

set runtimepath^=$XDG_CONFIG_HOME/vim
set runtimepath+=$XDG_DATA_HOME/vim
set runtimepath+=$XDG_CONFIG_HOME/vim/after

set packpath^=$XDG_DATA_HOME/vim,$XDG_CONFIG_HOME/vim
set packpath+=$XDG_CONFIG_HOME/vim/after,$XDG_DATA_HOME/vim/after

let g:netrw_home = $XDG_DATA_HOME."/vim"
call mkdir($XDG_DATA_HOME."/vim/spell", 'p', 0700)

set backupdir=$XDG_STATE_HOME/vim/backup | call mkdir(&backupdir, 'p', 0700)
set directory=$XDG_STATE_HOME/vim/swap   | call mkdir(&directory, 'p', 0700)
set undodir=$XDG_STATE_HOME/vim/undo     | call mkdir(&undodir,   'p', 0700)
set viewdir=$XDG_STATE_HOME/vim/view     | call mkdir(&viewdir,   'p', 0700)

if !has('nvim') " Neovim has its own special location
  set viminfofile=$XDG_STATE_HOME/vim/viminfo
endif

filetype plugin indent on
syntax enable
set backspace=indent,eol,start

" cursorshape
set t_SI=[6\ q
set t_EI=[2\ q
set t_Cs=[4:3m
set t_Ce=[4:0m

set tabstop=4
set shiftwidth=0
set softtabstop=-1
set expandtab
set mouse=n
set autowrite
set title
set list
set listchars=tab:│\ ,extends:»,precedes:«,nbsp:·,trail:·
set incsearch

set termguicolors
colorscheme quiet

set laststatus=2
set statusline=%<%F\ %h%m%r%=
set statusline+=%{statusline#git()}
set statusline+=%y
set statusline+=%{statusline#ff()}
set statusline+=%{statusline#fenc()}
set statusline+=\ %-10.(%l,%c%V%)\ %P"

set completeopt=menuone,noselect
inoremap <expr> <Return> pumvisible() ? "<C-Y>" : "<Return>"

augroup vimrc
  au!
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
    \ |   exe "normal! g`\""
    \ | endif
augroup END
