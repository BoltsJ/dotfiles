" Off is a mostly monochromatic colorscheme that provides highlighting only
" for UI elements and errors and dimming of comments.

" http://www.linusakesson.net/programming/syntaxhighlighting/
" http://kyleisom.net/blog/2012/10/17/syntax-off/
" https://web.archive.org/web/20201112021524/https://kyleisom.net/blog/2012/10/17/syntax-off/

" Pallet: ###########
" Color: #88ccee - 117
" Color: #332288 - 54
" Color: #44aa99 - 72
" Color: #117733 - 29
" Color: #999933 - 101
" Color: #ddcc77 - 186
" Color: #882255 - 89
" Color: #aa4499 - 132
" Greys: #############
" Color: #1a1a1a - 234
" Color: #333333 - 236
" Color: #4d4d4d - 239
" Color: #666666 - 241
" Color: #737373 - 243
" Color: #999999 - 246
" Color: #cccccc - 252

highlight clear
if exists('syntax_on')
  syntax reset
endif
let colors_name = 'off_vim'

" This is the important section. Nearly all syntax groups for code
" highlighting are linked to these.
hi Type gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Keyword gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Number gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Char gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Format gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Special gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Constant gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi PreProc gui=NONE guifg=NONE guibg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Directive gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Conditional gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Operator gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Delimiter gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Func gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Function gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Identifier gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi Statement gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE
hi String gui=NONE guibg=NONE guifg=NONE
      \ cterm=NONE ctermbg=NONE ctermfg=NONE

" Gui only
if !has('gui_running') && !(has('termguicolors') && &termguicolors)
  finish
endif

function! s:highlight(group, fg, bg='NONE', dec=v:null, sp=v:null) abort
  let l:hlc = 'hi! ' . a:group
  if a:fg == 'NONE'
    let l:hlc .= ' guifg=NONE ctermfg=NONE'
  else
    let l:hlc .= ' guifg=' . a:fg
  endif
  if a:bg == 'NONE'
    let l:hlc .= ' guibg=NONE ctermbg=NONE'
  else
    let l:hlc .= ' guibg=' . a:bg
  endif
  if a:dec != v:null
    let l:hlc .= ' gui='. a:dec . ' cterm=' . a:dec
  endif
  if a:sp != v:null
    let l:hlc .= ' guisp=' . a:sp
  endif
  exe l:hlc
endfunction

function! s:define_dark() abort
  call s:highlight('Normal', '#CCCCCC', '#1A1A1A')
  call s:highlight('NonText', '#545454')
  call s:highlight('Comment', '#666666')
  call s:highlight('EndOfBuffer', '#2E2E2E')

  highlight! link SignColumn LineNr
" call s:highlight('SignColumn')
  call s:highlight('ColorColumn', 'NONE', '#141414')
" call s:highlight('TabLine')
  highlight! link TabLine StatusLineNC
" call s:highlight('TabLineFill')
  highlight! link TabLineFill Normal
" call s:highlight('TabLineSel')
  highlight! link TabLineSel StatusLine
  call s:highlight('SpecialKey', '#4478AB')
" call s:highlight('IncSearch', 'NONE', 'NONE', 'underline')
  call s:highlight('Search', 'fg', 'NONE', 'reverse')
  call s:highlight('CurSearch', 'fg', '#141414', 'reverse,italic')
  call s:highlight('MoreMsg', '#44AB9A', 'NONE', 'bold')
  call s:highlight('ModeMsg', 'NONE', 'NONE', 'bold')
  call s:highlight('LineNr', '#666666', '#141414')
  call s:highlight('CursorLineNr', '#666666', '#141414', 'italic')
  call s:highlight('CursorColumn', 'NONE', '#303030')
  highlight! link CursorLine CursorColumn

  call s:highlight('StatusLine', 'bg', '#BABABA', 'bold')
  call s:highlight('StatusLineNC', 'bg', '#5E5E5E', 'NONE')
" call s:highlight('VertSplit')
  highlight! link VertSplit StatusLineNC
  call s:highlight('Title', '#A93276')
  call s:highlight('Visual', 'NONE', '#303030')
" call s:highlight('VisualNOS')
  call s:highlight('WarningMsg','bg', 'fg')
" call s:highlight('WildMenu')
  highlight! link WildMenu PmenuSel
  call s:highlight('Pmenu', '#68CDEE', '#303030')
  call s:highlight('PmenuSel', '#68CDEE', '#303030', 'reverse')
  call s:highlight('PmenuSBar', 'NONE', '#242424')
  call s:highlight('PmenuThumb', 'NONE', '#2B2B2B')
  call s:highlight('Folded', '#D6D6D6', '#303030')
" call s:highlight('FoldColumn')
  highlight! link FoldColumn Folded
  call s:highlight('Directory', '#68CDEE')
  call s:highlight('Italic', 'NONE', 'NONE', 'italic')
  call s:highlight('Underlined', 'NONE', 'NONE', 'underline')
  call s:highlight('ErrorMsg', 'NONE', '#A93276')
  call s:highlight('Error', '#A93276', 'NONE', 'underline')
" call s:highlight('Question')
  highlight! link Question MoreMsg
  call s:highlight('Todo', '#CBB943', 'bg', 'bold')
  call s:highlight('MatchParen', 'NONE', '#3D3D3D', 'bold')
  call s:highlight('SpellBad', 'NONE', 'NONE', 'undercurl', '#EE6877')
  call s:highlight('SpellCap', 'NONE', 'NONE', 'undercurl', '#4478AB')
  call s:highlight('SpellRare', 'NONE', 'NONE', 'undercurl', '#68CDEE')
  call s:highlight('SpellLocal', 'NONE', 'NONE', 'undercurl', '#CBB943')
  call s:highlight('DiffAdd', 'bg', '#68CDEE')
  call s:highlight('DiffChange', 'bg', '#CBB943')
  call s:highlight('DiffDelete', 'bg', '#EE6877')
  call s:highlight('DiffText', 'NONE', '#44AB9A')
endfunction

function! s:define_light() abort
  echomsg "LOL, Lmao"
  call s:highlight('Normal', '#1A1A1A', '#CCCCCC')
endfunction

if &background == 'dark'
  call s:define_dark()
else
  call s:define_light()
endif

" vim:sts=2:ts=2:sw=2:et:
