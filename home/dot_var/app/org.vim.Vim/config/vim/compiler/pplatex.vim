" Vim compiler file
" Compiler:         pplatex compiler

if exists('current_compiler')
    finish
endif
let current_compiler = 'pplatex'

let s:save_cpo = &cpo
set cpo&vim

if exists(':CompilerSet') != 2
    command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet mp=pplatex\ -i\ %:r.log

" Hijacked from:
" https://github.com/lervag/vimtex/blob/master/autoload/vimtex/qf/pplatex.vim

" Start of new items with file and line number, message on next line(s).
CompilerSet errorformat=%E**\ Error\ \ \ in\ %f\\,\ Line\ %l:\ 
CompilerSet errorformat+=%W**\ Warning\ in\ %f\\,\ Line\ %l:\ 
CompilerSet errorformat+=%I**\ BadBox\ \ in\ %f\\,\ Line\ %l:\ 

" Start of items with with file, line and message on the same line. There are
" no BadBoxes reported this way.
CompilerSet errorformat+=%E**\ Error\ \ \ in\ %f\\,\ Line\ %l:\ %m
CompilerSet errorformat+=%W**\ Warning\ in\ %f\\,\ Line\ %l:\ %m

" Start of new items with only a file.
CompilerSet errorformat+=%E**\ Error\ \ \ in\ %f:\ 
CompilerSet errorformat+=%W**\ Warning\ in\ %f:\ 
CompilerSet errorformat+=%I**\ BadBox\ \ in\ %f:\ 

" Start of items with with file and message on the same line. There are
" no BadBoxes reported this way.
CompilerSet errorformat+=%E**\ Error\ in\ %f:\ %m
CompilerSet errorformat+=%W**\ Warning\ in\ %f:\ %m

" Anything that starts with three spaces is part of the message from a
" previously started multiline error item.
CompilerSet errorformat+=%C\ \ \ %m\ on\ input\ line\ %l.
CompilerSet errorformat+=%C\ \ \ %m

" Items are terminated with two newlines.
CompilerSet errorformat+=%-Z

" Skip statistical results at the bottom of the output.
CompilerSet errorformat+=%-GResult%.%#
CompilerSet errorformat+=%-G

let &cpo = s:save_cpo
unlet s:save_cpo
