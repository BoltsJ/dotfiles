" Vim compiler file
" Compiler:         Perl6 compiler
" Maintainer:       Joseph R. Nosie
" Latest Revision:  2017 Nov 16

if exists('current_compiler')
    finish
endif
let current_compiler = "perl6"

let s:save_cpo = &cpo
set cpo&vim

if exists(':CompilerSet') != 2
    command -nargs=* CompilerSet setlocal <args>
endif

let $RAKUDO_ERROR_COLOR=0

CompilerSet mp=perl6\ -c\ %\ $*

CompilerSet errorformat=
            \%E===SORRY!===\ Error\ while\ compiling\ %f,
            \%+EMalformed\ %m,
            \%+W\ %#Quotes\ are\ not\ %m,
            \%+W\ %#Redeclaration\ %m,
            \%+W\ %#Repeated\ character\ %m,
            \%+W\ %#Space\ is\ not\ %m,
            \%+W\ %#Useless\ %m,
            \%C\ %#at\ %f:%l,
            \%C\ %#%m\ used\ at\ line\ %l,
            \%C\ %#%m,
            \%Z%m,
            \%-G,
            \%-G===SORRY!===,
            \%-GPotential\ difficulties:,
            \%-GSyntax\ OK

let &cpo = s:save_cpo
unlet s:save_cpo
