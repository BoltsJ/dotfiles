" Vim compiler file
" Compiler:         Perl6 compiler
" Maintainer:       Joseph R. Nosie
" Latest Revision:  2017 Nov 16

if exists('current_compiler')
    finish
endif
let current_compiler = 'vint'

let s:save_cpo = &cpo
set cpo&vim

if exists(':CompilerSet') != 2
    command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet mp=vint\ --style\ %

CompilerSet errorformat=
      \%f:%l:%c:\ %t%n:%m,
      \%f:%l:%c:%m\ (see\ :help\ %t%n),
      \%W%f:%l:%c:%m

let &cpo = s:save_cpo
unlet s:save_cpo
