function! qf#open_qf() abort
  let l:cw = win_getid()
  execute 'cclose|botright '.min([10, len(getqflist())]).'cwindow'
  call win_gotoid(l:cw)
endfunction

function! qf#open_ll() abort
  let l:cw = win_getid()
  execute 'lclose|'.min([10, len(getloclist(0))]).'lwindow'
  call win_gotoid(l:cw)
endfunction
