function! dim#enter() abort
  if exists('b:old_syntax')
    exe 'set syntax=' . b:old_syntax
  endif
  setlocal wincolor=
endfunction

function! dim#leave() abort
  if !exists('b:nodim') && &syntax != 'OFF'
    let b:old_syntax = &syntax
    set syntax=OFF
    set wincolor=Comment
  endif
endfunction
