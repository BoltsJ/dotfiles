function! statusline#ff() abort
  return &fileformat !=# 'unix' ? '['.&fileformat.']' : ''
endfunction

function! statusline#fenc() abort
  let l:ignore = ['', 'utf-8']
  return index(l:ignore, &fileencoding) < 0 ? '['.&fileencoding.']' : ''
endfunction

function! statusline#git() abort
  if !exists('g:loaded_fugitive')
    return ''
  endif
  " let l:branch = fugitive#head(8)
  let l:branch = FugitiveHead(8)
  return l:branch !=? '' ? '[git:'.l:branch.']' : ''
endfunction
