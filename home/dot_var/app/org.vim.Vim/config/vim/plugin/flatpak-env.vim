if exists('g:flatpak_env')
  finish
endif
let g:flatpak_env=1
if !filereadable('/.flatpak-info') | finish | endif

if isdirectory(expand('~/.var/app/org.vim.Vim/bin'))
  let $PATH=expand('~/.var/app/org.vim.Vim/bin').':'.$PATH
endif

" node18
if filereadable('/usr/lib/sdk/node18/enable.sh')
  let $NPM_CONFIG_GLOBALCONFIG=expand('<sfile>:p:h:h').'/npm/npmrc'
  let $PATH.=':'.expand('$XDG_DATA_HOME/node_modules/bin')
  let $PATH.=':/usr/lib/sdk/node18/bin'
  let $npm_config_nodedir='/usr/lib/sdk/node18'
endif
