if exists('g:loaded_autoqf')
  finish
endif
let g:loaded_autoqf=1

augroup auto_qf
autocmd!
autocmd QuickfixCmdPost [^l]* call qf#open_qf()
autocmd QuickfixCmdPost l* call qf#open_ll()
augroup END
