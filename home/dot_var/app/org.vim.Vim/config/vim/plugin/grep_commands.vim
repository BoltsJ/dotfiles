if exists('g:loaded_grep_commands')
  finish
endif
let g:loaded_grep_commands=1

if executable('rg')
  set grepprg=rg\ -P\ -S\ --vimgrep\ $*
  set grepformat=%f:%l:%c:%m
elseif executable('ag')
  set grepprg=ag\ -f\ --vimgrep\ $*
  set grepformat=%f:%l:%c:%m
endif
" TODO: Figure out how to avoid :redraw!
command! -nargs=+ -bar Grep silent! grep! '<args>' | redraw!
command! -nargs=+ -bar LGrep silent! lgrep! '<args>' | redraw!
