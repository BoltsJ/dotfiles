if exists('g:sudoedit_toolbox_host')
  finish
endif
let g:sudoedit_toolbox_host=1

function! s:toolbox_host()
  let l:current_buf = bufnr()
  exe 'edit /var/run/host' . l:current_buf->bufname()
  exe 'bwipeout ' . l:current_buf
endfunction

command! -nargs=0 SudoeditHost call <SID>toolbox_host()
