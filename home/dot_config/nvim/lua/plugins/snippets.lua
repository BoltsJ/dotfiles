return {
  "L3MON4D3/LuaSnip",
  version = "v2.*",
  event = "VeryLazy",
  keys = {
    {
      "<tab>",
      function() return require("luasnip").expand_or_jumpable(1) and "<Plug>luasnip-expand-or-jump" or "<tab>" end,
      expr = true,
      silent = true,
      mode = "i",
      desc = "Expand luasnip snippet",
    },
    {
      "<tab>",
      function() require("luasnip").jump(1) end,
      mode = "s",
      desc = "Jump to next luasnip node",
    },
    {
      "<s-tab>",
      function() require("luasnip").jump(-1) end,
      mode = { "i", "s" },
      desc = "Jump to previous luasnip node",
    },
  },
  build = "make install_jsregexp",
  dependencies = {
    "rafamadriz/friendly-snippets",
    config = function() require("luasnip.loaders.from_vscode").lazy_load() end,
  },
  opts = {
    region_check_events = "InsertEnter",
    delete_check_events = "TextChanged",
  },
}
