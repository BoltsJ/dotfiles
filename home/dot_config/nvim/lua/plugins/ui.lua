return {
  {
    "lukas-reineke/indent-blankline.nvim",
    event = "VeryLazy",
    main = "ibl",
    opts = {
      indent = { char = { "▏" } },
      scope = { show_start = false, show_end = false },
    },
  },
  { "stevearc/dressing.nvim" },
  {
    "j-hui/fidget.nvim",
    event = "VeryLazy",
    opts = {
      notification = { override_vim_notify = true },
    },
  },
  {
    "uga-rosa/ccc.nvim",
    version = "*",
    cond = vim.opt.termguicolors,
    ft = { "css", "scss", "svelte" },
    cmd = { "CccConvert", "CccHighlighterEnable", "CccHighlighterToggle", "CccPick" },
    opts = {
      highlighter = {
        auto_enable = true,
        filetypes = { "css", "scss", "svelte" },
      },
    },
  },
}
