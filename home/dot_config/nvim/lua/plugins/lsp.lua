return {
  "neovim/nvim-lspconfig",
  dependencies = {
    {
      "onsails/lspkind-nvim",
      config = function() require("lspkind").init() end,
    },
    {
      "williamboman/mason-lspconfig.nvim",
      version = "*",
      dependencies = { "williamboman/mason.nvim", opts = {} },
      opts = function()
        return {
          ensure_installed = { "lua_ls" },
          handlers = {
            function(server)
              local servers = require("config.lsp.servers")
              local config = servers[server] or {}
              config = vim.tbl_deep_extend("force", {
                handlers = require("config.lsp.handlers"),
              }, config)
              require("lspconfig")[server].setup(config)
            end,
          },
        }
      end,
    },
  },
  event = { "FileType" },
  init = function()
    vim.diagnostic.config({
      signs = false,
      severity_sort = true,
    })
  end,
  config = function()
    local augroup = vim.api.nvim_create_augroup("LspConfig", { clear = true })
    vim.api.nvim_create_autocmd("LspAttach", {
      group = augroup,
      callback = require("config.lsp.attach").callback,
    })
  end,
}
