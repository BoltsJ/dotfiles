return {
  "stevearc/conform.nvim",
  dependencies = { "williamboman/mason.nvim", opts = {} },
  -- event = { "BufWritePre" },
  cmd = { "ConformInfo" },
  init = function()
    vim.api.nvim_create_user_command("Format", function(args)
      local range = nil
      if args.count ~= -1 then
        local end_line = vim.api.nvim_buf_get_lines(0, args.line2 - 1, args.line2, true)[1]
        range = {
          start = { args.line1, 0 },
          ["end"] = { args.line2, end_line:len() },
        }
      end
      require("conform").format({ async = true, lsp_fallback = true, range = range })
    end, { range = true })
  end,
  opts = {
    formatters_by_ft = {
      bib = { "bibtex-tidy" },
      css = { "prettier" },
      javascript = { "prettier" },
      json = { { "prettier", "jq" } },
      lua = { "stylua" },
      scss = { "prettier" },
      svelte = { "prettier" },
      typescript = { "prettier" },
    },
  },
}
