return {
  "ibhagwan/fzf-lua",
  keys = {
    { "<Bslash>ff", function() require("fzf-lua").files() end, desc = "Fzf-lua files" },
    { "<Bslash>fg", function() require("fzf-lua").live_grep() end, desc = "Fzf-lua live grep" },
  },
  cmd = "FzfLua",
}
