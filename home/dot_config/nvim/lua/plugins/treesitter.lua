return {
  "nvim-treesitter/nvim-treesitter",
  event = { "VeryLazy", "FileType" },
  build = ":TSUpdate",
  main = "nvim-treesitter.configs",
  opts = {
    ensure_installed = { "vim", "lua", "query", "vimdoc", "c" },
    highlight = { enable = true },
  },
}
