return {
  {
    "stevearc/oil.nvim",
    enabled = false,
    opts = {},
    keys = {
      { "-", "<CMD>Oil<CR>", desc = "Open parent directory" },
    },
    cmd = { "Oil" },
  },
  { "romainl/vim-cool", event = "VeryLazy" },
  {
    "tpope/vim-commentary",
    dependencies = {
      "JoosepAlviste/nvim-ts-context-commentstring",
      init = function() vim.g.skip_ts_context_commentstring_module = true end,
    },
  },
  { "tpope/vim-fugitive", cmd = { "G", "Git", "Gvdiffsplit" }, event = "VeryLazy" },
  { "tpope/vim-unimpaired", event = "VeryLazy" },
  {
    "tpope/vim-vinegar",
    init = function() vim.g.netrw_fastbrowse = 0 end,
  },
  { "vimpostor/vim-lumen" },
  {
    "rktjmp/shipwright.nvim",
    dependencies = { "rktjmp/lush.nvim", cmd = "Lushify" },
    cmd = "Shipwright",
  },
  {
    "nvim-tree/nvim-web-devicons",
    lazy = true,
    opts = {
      override = {
        ["netrw"] = {
          icon = "",
          color = "#ab8814",
          cterm_color = "136",
          name = "Netrw",
        },
      },
    },
  },
}
