---@diagnostic disable: undefined-field
return {
  "mfussenegger/nvim-dap",
  dependencies = {
    {
      "rcarriga/nvim-dap-ui",
      dependencies = { "nvim-neotest/nvim-nio" },
      config = function(_, opts)
        local dap, dapui = require("dap"), require("dapui")
        dapui.setup(opts)
        dap.listeners.after.event_initialized["dapui_config"] = function() dapui.open() end
        dap.listeners.before.event_terminated["dapui_config"] = function() dapui.close() end
        dap.listeners.before.event_exited["dapui_config"] = function() dapui.close() end
      end,
    },
    {
      "jay-babu/mason-nvim-dap.nvim",
      dependencies = { "williamboman/mason.nvim", opts = {} },
      opts = {
        handlers = {
          function(config) require("mason-nvim-dap").default_setup(config) end,
        },
      },
    },
  },
  lazy = true,
  config = function()
    local dap = require("dap")
    require("dap.ext.vscode").load_launchjs()
    dap.set_log_level("DEBUG")

    -- TODO: Find a way to move parts of this config to project directory
    dap.configurations.javascript = {
      {
        name = "Debug with Firefox",
        type = "firefox",
        request = "launch",
        reAttach = true,
        url = "http://localhost:30001/",
        webRoot = "${workspaceFolder}/src",
        -- firefoxExecutable = os.getenv "HOME" .. "/.var/app/io.neovim.nvim/bin/ff-wrapper",
        firefoxExecutable = os.getenv("HOME") .. "/.local/bin/ff-wrapper",
      },
    }
    dap.configurations.typescript = {
      {
        name = "Debug with Firefox",
        type = "firefox",
        request = "launch",
        reAttach = true,
        url = "http://localhost:30001/systems/lancer/index.html",
        webRoot = "${workspaceFolder}/src",
        -- firefoxExecutable = os.getenv "HOME" .. "/.var/app/io.neovim.nvim/bin/ff-wrapper",
        firefoxExecutable = os.getenv("HOME") .. "/.local/bin/ff-wrapper",
      },
    }
  end,
}
