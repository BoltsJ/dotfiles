return {
  {
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = "markdown",
    build = function() vim.fn["mkdp#util#install"]() end,
  },
  {
    "lervag/vimtex",
    -- enabled = false,
    ft = { "tex", "plaintex", "latex", "bib", "bibtex" },
    init = function()
      -- vim.g.tex_flavor = "latex"
      -- vim.g.vimtex_compiler_enabled = 0
      vim.g.vimtex_compiler_method = "tectonic"
      vim.g.vimtex_imaps_enabled = 0
      vim.g.vimtex_labels_enabled = 0
      vim.g.vimtex_quickfix_enabled = 0
      vim.g.vimtex_toc_enabled = 0
      vim.g.vimtex_view_method = "zathura"
      -- vim.g.vimtex_view_general_options = "--unique file:@pdf#src:@line@tex"
      -- vim.g.vimtex_view_general_viewer = "xdg-open"
    end,
  },
}
