return {
  "stevearc/overseer.nvim",
  keys = {
    { "<Bslash>ot", function() require("overseer").toggle() end, desc = "Toggle overseer task list" },
    { "<Bslash>or", function() require("overseer").run_template() end, desc = "Start overseer task" },
  },
  cmd = "OverseerRun",
  opts = {},
}
