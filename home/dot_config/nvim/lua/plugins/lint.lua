return {
  "mfussenegger/nvim-lint",
  lazy = true,
  init = function()
    vim.api.nvim_create_autocmd({ "BufWritePost", "BufReadPost" }, {
      group = vim.api.nvim_create_augroup("nvim-lint", { clear = true }),
      callback = function() require("util.lint").lint() end,
    })
  end,
  opts = {
    linters_by_ft = {
      javascript = { "eslint" },
      svelte = { "eslint" },
    },
    linters = {
      eslint = {
        condition = function(ctx)
          return #vim.fs.find(
            { ".eslintrc.js", ".eslintrc.cjs", ".eslintrc.yaml", ".eslintrc.yml", ".eslintrc.json", "package.json" },
            { path = ctx.filename, upward = true }
          ) > 0
        end,
      },
    },
  },
  config = function(_, opts)
    ---@diagnostic disable-next-line: different-requires
    local lint = require("lint")
    for name, linter in pairs(opts.linters) do
      if type(linter) == "table" and type(lint.linters[name]) == "table" then
        lint.linters[name] = vim.tbl_deep_extend("force", lint.linters[name], linter)
      else
        lint.linters[name] = linter
      end
    end
    lint.linters_by_ft = opts.linters_by_ft
  end,
}
