local location_handler = function(_, result, ctx, _)
  if not result or vim.tbl_isempty(result) then
    local _ = vim.log.info() and vim.lsp.log.info(ctx.method, "No location found")
    return nil
  end
  local client = vim.lsp.get_client_by_id(ctx.client_id)

  if vim.tbl_islist(result) then
    vim.lsp.util.jump_to_location(result[1], client.offset_encoding)

    if #result > 1 then
      vim.api.nvim_exec_autocmds("QuickfixCmdPre", { pattern = "lreferences" })
      vim.fn.setloclist(0, {}, " ", {
        title = "Language Server",
        items = vim.lsp.util.locations_to_items(result, client.offset_encoding),
        context = ctx,
      })
      vim.api.nvim_exec_autocmds("QuickfixCmdPost", { pattern = "lreferences" })
    end
  else
    vim.lsp.util.jump_to_location(result, client.offset_encoding)
  end
end

local custom_handlers = {
  ["textDocument/declaration"] = location_handler,
  ["textDocument/definition"] = location_handler,
  ["textDocument/typeDefinition"] = location_handler,
  ["textDocument/implementation"] = location_handler,
  ["textDocument/references"] = function(_, result, ctx, config)
    if not result then vim.notify("No references found") end
    local client = vim.lsp.get_client_by_id(ctx.client_id)
    config = config or {}
    vim.api.nvim_exec_autocmds("QuickfixCmdPre", { pattern = "lreferences" })
    vim.fn.setloclist(0, {}, " ", {
      title = "References",
      items = vim.lsp.util.locations_to_items(result, client.offset_encoding),
      context = ctx,
    })
    vim.api.nvim_exec_autocmds("QuickfixCmdPost", { pattern = "lreferences" })
  end,
}

return custom_handlers
