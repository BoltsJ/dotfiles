local callback = function(args)
  local client = vim.lsp.get_client_by_id(args.data.client_id)

  vim.api.nvim_buf_create_user_command(args.buf, "LspHover", vim.lsp.buf.hover, { nargs = "*" })
  vim.bo[args.buf].keywordprg = ":LspHover"

  local opts = { buffer = args.buf }
  vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
  vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
  vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
  vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, opts)
  vim.keymap.set("n", "<space>wa", vim.lsp.buf.add_workspace_folder, opts)
  vim.keymap.set("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, opts)
  vim.keymap.set("n", "<space>wl", function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, opts)
  vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, opts)
  vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, opts)
  vim.keymap.set({ "n", "v" }, "<space>ca", vim.lsp.buf.code_action, opts)
  vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)

  vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, opts)

  -- Diagnostics
  vim.keymap.set("n", "<space>q", function() require("util.lsp").qflist_diagnostics() end, opts)
  vim.keymap.set("n", "<space>d", vim.diagnostic.open_float, opts)
  vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
  vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)

  if client.server_capabilities.documentHighlightProvider then
    local augroup = vim.api.nvim_create_augroup("lsp_document_highlight", {})
    vim.api.nvim_clear_autocmds({ buffer = args.buf, group = augroup })
    vim.api.nvim_create_autocmd("CursorHold", {
      group = augroup,
      buffer = args.buf,
      callback = vim.lsp.buf.document_highlight,
    })
    vim.api.nvim_create_autocmd("CursorMoved", {
      group = augroup,
      buffer = args.buf,
      callback = vim.lsp.buf.clear_references,
    })
  end

  if client.server_capabilities.code_lens then
    vim.cmd([[
    augroup lsp_document_codelens
    autocmd! * <buffer>
    autocmd BufWritePost,CursorHold <buffer> lua vim.lsp.codelens.refresh()
    augroup END
    ]])
  end
end

return { callback = callback }
