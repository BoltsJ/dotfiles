local M = {}

local active = function() return vim.api.nvim_get_current_win() == tonumber(vim.g.statusline_winid) end

local get_buf = function() return vim.api.nvim_win_get_buf(vim.g.statusline_winid) end

M.file_icon = function(buf)
  local use_icons, devicons = pcall(require, "nvim-web-devicons")
  if not use_icons then return "" end

  buf = buf or get_buf()
  local fname = vim.fn.fnamemodify(vim.api.nvim_buf_get_name(buf), ":t")
  local ftype = vim.bo[buf].filetype
  -- local fext = vim.fn.fnamemodify(fname, ":e")
  if ftype == "fugitive" then ftype = "git" end
  local icon, color = devicons.get_icon_color(fname)
  if ftype == "netrw" or ftype == "checkhealth" or ftype == "query" then
    icon, color = devicons.get_icon_color(ftype)
  end
  if icon == nil then
    icon, color = devicons.get_icon_color_by_filetype(ftype, { default = true })
  end
  local bg = vim.api.nvim_get_hl(0, { name = "StatusLine" }).bg

  if vim.bo[buf].buftype == "help" then
    icon = "󰋖"
    color = "#b22768"
  end

  vim.api.nvim_set_hl(0, "StatusFileIcon", { fg = color, bg = bg })

  local hl = active() and "%#StatusFileIcon#" or ""

  return hl .. icon .. "%* "
end

M.git = function(buf)
  buf = buf or get_buf()
  if vim.g.loaded_fugitive == nil then return "" end

  local branch = vim.fn["FugitiveHead"](8, buf)
  local hl = active() and "%#StatusLineGit#" or ""
  if branch ~= "" then
    return hl .. " " .. branch .. "%* "
  else
    return ""
  end
end

M.fugitive_buffer = function(buf)
  buf = buf or get_buf()
  if vim.b[buf].fugitive_type == "blob" then return " " end
  return ""
end

M.modified = function(buf)
  buf = buf or get_buf()
  local bg = vim.api.nvim_get_hl(0, { name = "StatusLine" }).bg
  vim.api.nvim_set_hl(0, "StatusFileModified", { fg = "#ff8c00", bg = bg })

  local hl = active() and "%#StatusFileModified#" or ""

  if vim.bo[buf].modifiable and vim.bo[buf].modified and vim.bo[buf].buftype == "" then
    return hl .. "󰳻 %*"
  elseif not vim.bo[buf].modifiable or vim.bo[buf].readonly then
    return hl .. "󰌾 %*"
  else
    return ""
  end
end

M.ff = function(buf)
  buf = buf or get_buf()
  if vim.bo[buf].fileformat == "dos" then
    return "CRLF "
  elseif vim.bo[buf].fileformat == "mac" then
    return "CR "
  elseif vim.bo[buf].fileformat == "unix" then
    return ""
  end
end

M.fenc = function(buf)
  buf = buf or get_buf()
  return (vim.bo[buf].fileencoding ~= "utf-8" and string.upper(vim.bo[buf].fileencoding) .. " " or "")
end

M.diagnostics = function(buf)
  buf = buf or get_buf()
  local diags = vim.diagnostic.get(buf)
  local counts = {
    [vim.diagnostic.severity.ERROR] = 0,
    [vim.diagnostic.severity.WARN] = 0,
    [vim.diagnostic.severity.INFO] = 0,
    [vim.diagnostic.severity.HINT] = 0,
  }
  for _, v in ipairs(diags) do
    counts[v.severity] = counts[v.severity] + 1
  end

  local ret = ""
  if counts[vim.diagnostic.severity.ERROR] > 0 then
    ret = ret
      .. (active() and "%#DiagnosticStatusError#" or "")
      .. " "
      .. counts[vim.diagnostic.severity.ERROR]
      .. " "
  end
  if counts[vim.diagnostic.severity.WARN] > 0 then
    ret = ret .. (active() and "%#DiagnosticStatusWarn#" or "") .. " " .. counts[vim.diagnostic.severity.WARN] .. " "
  end
  if counts[vim.diagnostic.severity.INFO] > 0 then
    ret = ret .. (active() and "%#DiagnosticStatusInfo#" or "") .. " " .. counts[vim.diagnostic.severity.INFO] .. " "
  end
  if counts[vim.diagnostic.severity.HINT] > 0 then
    ret = ret .. (active() and "%#DiagnosticStatusHint#" or "") .. " " .. counts[vim.diagnostic.severity.HINT] .. " "
  end
  return ret .. "%*"
end

M.status = function()
  local buf = get_buf()
  return " "
    .. M.file_icon(buf)
    .. M.modified(buf)
    .. M.fugitive_buffer(buf)
    .. "%t  %(%l:%c%V%)  %P%="
    .. M.ff(buf)
    .. M.fenc(buf)
    .. vim.bo[buf].filetype
    .. " "
    .. M.git(buf)
    .. M.diagnostics(buf)
end

return M
