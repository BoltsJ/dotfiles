local M = {}

M.loclist_diagnostics = function()
  vim.cmd([[ doautocmd QuickfixCmdPre ldiagnostic ]])
  vim.diagnostic.setloclist({ open = false })
  vim.cmd([[ doautocmd QuickfixCmdPost ldiagnostic ]])
end

M.qflist_diagnostics = function()
  vim.cmd([[ doautocmd QuickfixCmdPre diagnostic ]])
  vim.diagnostic.setqflist({ open = false })
  vim.cmd([[ doautocmd QuickfixCmdPost diagnostic ]])
end

return M
