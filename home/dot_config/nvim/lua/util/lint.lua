local M = {}

-- Shamelessly copied from LazyVim
function M.lint()
  local lint = require("lint")

  local names = lint._resolve_linter_by_ft(vim.bo.filetype)
  if #names == 0 then vim.list_extend(names, lint.linters_by_ft["_"] or {}) end
  vim.list_extend(names, lint.linters_by_ft["*"] or {})

  local ctx = { filename = vim.api.nvim_buf_get_name(0) }
  ctx.dirname = vim.fn.fnamemodify(ctx.filename, ":h")
  names = vim.tbl_filter(function(name)
    local linter = lint.linters[name]
    local cmd = type(linter.cmd) == "function" and linter.cmd() or linter.cmd
    return linter
      and not (type(linter) == "table" and linter.condition and not linter.condition(ctx))
      and vim.fn.executable(cmd) == 1
  end, names)

  if #names > 0 then lint.try_lint(names) end
end

return M
