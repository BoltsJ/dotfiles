---@diagnostic disable: undefined-global
--
-- Built with,
--
--        ,gggg,
--       d8" "8I                         ,dPYb,
--       88  ,dP                         IP'`Yb
--    8888888P"                          I8  8I
--       88                              I8  8'
--       88        gg      gg    ,g,     I8 dPgg,
--  ,aa,_88        I8      8I   ,8'8,    I8dP" "8I
-- dP" "88P        I8,    ,8I  ,8'  Yb   I8P    I8
-- Yb,_,d88b,,_   ,d8b,  ,d8b,,8'_   8) ,d8     I8,
--  "Y8P"  "Y888888P'"Y88P"`Y8P' "YY8P8P88P     `Y8
--

local lush = require "lush"
local hsl = lush.hsl

local normalfg = hsl(0, 0, 80)
local normalbg = hsl(0, 0, 5)
local wine = hsl(326, 54, 43)
local red = hsl(353, 80, 67)
local yellow = hsl(52, 57, 53)
--local green    = hsl(130,60,33)
local green = hsl "#44aa99"
local cyan = hsl(195, 80, 67)
local blue = hsl(210, 43, 47)
---@diagnostic disable-next-line: unused-local
local indigo = hsl(250, 60, 33)

local theme = lush(function(injected_functions)
  local sym = injected_functions.sym
  return {
    Normal { fg = normalfg, bg = normalbg, sp = normalfg },
    -- Syntax
    Constant {},
    Operator {},
    PreProc {},
    Type {},
    Delimiter {},
    Comment { fg = Normal.fg.darken(50), sp = Normal.fg.darken(50) },
    String { fg = Comment.fg, gui = "italic", sp = Comment.sp },
    Identifier {},
    Function {},
    Statement {},
    Special {},
    NormalNC { Comment },

    Underlined { gui = "underline" },
    Bold { gui = "bold" },
    Italic { gui = "italic" },

    SignColumn { fg = Normal.bg.lighten(20) },
    LineNr { SignColumn },
    CursorLineNr { LineNr, gui = "bold" },
    ColorColumn { bg = Normal.bg.lighten(10) },
    EndOfBuffer { fg = Comment.fg.darken(55) },
    Visual { bg = Normal.bg.lighten(10) },
    Search { gui = "reverse,bold" },
    CurSearch { Search, fg=blue},
    MatchParen { gui = "bold", bg = Normal.bg.lighten(15) },
    IncSearch { gui = "reverse" },
    Substitute { gui = "reverse" },

    NonText { fg = Normal.bg.desaturate(25).lighten(25) },
    Whitespace { NonText },
    Conceal { fg = Comment.fg.lighten(10), gui = "bold" },
    SpecialKey { gui = "bold", fg = blue },

    Pmenu { bg = Normal.bg.lighten(10), fg = cyan },
    PmenuSel { Pmenu, gui = "reverse" },
    PmenuSbar { bg = Pmenu.bg.darken(25) },
    PmenuThumb { bg = Pmenu.bg.darken(10) },
    WildMenu { PmenuSel },

    NormalFloat { Normal, bg = blue.darken(65) },
    -- FloatShadow {},
    -- FloatShadowThrough {},

    Title { fg = blue, gui="bold" },
    StatusLine { gui = "bold", bg = Normal.bg.lighten(5) },
    StatusLineNC { fg = Normal.fg.darken(50), bg = StatusLine.bg },
    Winbar { StatusLine },
    WinbarNC { StatusLineNC },
    TabLine { StatusLineNC },
    TabLineFill { Normal },
    TabLineSel { StatusLine },
    VertSplit { fg = StatusLineNC.bg, bg = Normal.bg },
    Winseparator { VertSplit },
    Folded { bg = Normal.bg.lighten(10), fg = Normal.fg.lighten(20) },
    FoldColumn { Folded },

    MoreMsg { gui = "bold", fg = green },
    MsgArea {},
    MsgSeparator {},
    Question { MoreMsg },
    ModeMsg { gui = "bold" },

    Directory { fg = cyan },

    DiffAdd { bg = cyan.darken(60) },
    DiffChange { bg = yellow.darken(60) },
    DiffDelete { bg = red.darken(60) },
    DiffText { gui = "bold", bg = green.darken(60) },

    SpellBad { gui = "undercurl", sp = red },
    SpellCap { gui = "undercurl", sp = blue },
    SpellRare { gui = "undercurl", sp = cyan },
    SpellLocal { gui = "undercurl", sp = yellow },

    Cursor { gui = "reverse" },
    CursorColumn { bg = Normal.bg.lighten(10) },
    CursorLine { CursorColumn },
    QuickFixLine { bg = CursorLine.bg },
    qfLineNr { Comment },
    TermCursor { gui = "reverse" },
    TermCursorNC { gui = "reverse" },
    ErrorMsg { bg = wine },
    WarningMsg { gui = "reverse" },

    Error { gui = "underline", fg = wine, sp = wine },
    Todo { gui = "bold", bg = yellow.darken(70), fg = yellow },

    -- RedrawDebugClear {},
    -- RedrawDebugComposed {},
    -- RedrawDebugRecompose {},

    Added { fg = cyan},
    Changed { fg = yellow },
    Removed { fg = red },

    -- Built in LSP
    DiagnosticError { fg = wine, sp = wine },
    DiagnosticSignError { fg = DiagnosticError.fg, bg = SignColumn.bg },
    DiagnosticStatusError { fg = DiagnosticError.fg, bg = StatusLine.bg },
    DiagnosticUnderlineError { gui = "undercurl", sp = DiagnosticError.fg },
    DiagnosticWarn { fg = yellow, sp = yellow },
    DiagnosticSignWarn { fg = DiagnosticWarn.fg, bg = SignColumn.bg },
    DiagnosticStatusWarn { fg = DiagnosticWarn.fg, bg = StatusLine.bg },
    DiagnosticUnderlineWarn { gui = "undercurl", sp = DiagnosticWarn.fg },
    DiagnosticInfo { fg = cyan, sp = cyan },
    DiagnosticSignInfo { fg = DiagnosticInfo.fg, bg = SignColumn.bg },
    DiagnosticStatusInfo { fg = DiagnosticInfo.fg, bg = StatusLine.bg },
    DiagnosticUnderlineInfo { gui = "undercurl", sp = DiagnosticInfo.fg },
    DiagnosticHint { fg = Comment.fg, sp = Comment.fg },
    DiagnosticSignHint { fg = DiagnosticHint.fg, bg = SignColumn.bg },
    DiagnosticStatusHint { fg = DiagnosticHint.fg, bg = StatusLine.bg },
    DiagnosticUnderlineHint { gui = "undercurl", sp = DiagnosticHint.fg },
    DiagnosticOk { fg = blue },
    DiagnosticUnderlineOk { gui = "underline", sp = DiagnosticOk.fg },
    DiagnosticDeprecated { gui = "strikethrough", sp = DiagnosticError.fg },
    LspReferenceText { gui = "underline,italic" },
    LspReferenceRead { LspReferenceText },
    LspReferenceWrite { LspReferenceText },
    LspCodeLens { LineNr },
    LspCodeLensSeparator { LspCodeLens },

    -- Statusline
    StatusLineGit { fg = green, bg = StatusLine.bg },

    -- LuaSnip
    LuasnipChoice { fg = cyan },
    LuasnipInsert { fg = yellow },
    LuasnipNode { Visual, gui = "italic" },

    -- Vim help
    helpSpecial { fg = cyan },
    helpHyperTextJump { gui = "underline", fg = cyan, sp = cyan },
    helpHeadline { Title },

    -- Rust
    rustCommentLineDoc { String },
    rustAttribute { Comment },

    IblIndent { fg = EndOfBuffer.fg, gui = "nocombine" },
    IblScope { fg = Comment.fg },
    -- IblWhitespace { },
    -- IndentBlankLineContextChar        { gui="nocombine" },
    -- IndentBlankLineSpaceChar          { },
    -- IndentBlankLineSpaceCharBlankline { },

    -- Fugitive
    fugitiveHeader { gui = "bold" },
    fugitiveHeading { fugitiveHeader },
    fugitiveCount { String },
    fugitiveHash { Comment },
    fugitiveSymbolicRef { gui = "italic" },
    fugitiveHelpTag { gui = "italic" },
    fugitiveUntrackedHeading { gui = "bold" },
    fugitiveUntrackedModifier { fg = yellow },
    fugitiveUntrackedSection { fugitiveCount },
    fugitiveUnstagedHeading { gui = "bold" },
    fugitiveUnstagedModifier { fg = red },
    fugitiveUnstagedSection { fugitiveCount },
    fugitiveStagedHeading { gui = "bold" },
    fugitiveStagedModifier { fg = cyan },
    fugitiveStagedSection { fugitiveCount },
    gitCommitOverflow { Error },

    -- Treesitter
    sym"@variable" {},
    sym"@text.strike" { gui = "strikethrough" },
    sym"@text.underline" { gui = "underline" },
    sym"@text.emphasis" { gui = "italic" },
    sym"@text.strong" { gui = "bold" },
    sym"@text.reference" { gui = "underline", fg = cyan, sp = cyan },
    sym"@string.special.vimdoc" { fg = cyan },
    sym"@label.vimdoc" { String },

    sym"@markup.link.label.markdown_inline" { sym"@text.reference" },
    sym"@markup.link.markdown_inline" { String },

    sym"@neorg.links.description" { sym"@text.reference" },
    sym"@neorg.headings.1.title" { Title, gui = "bold,underline" },
    sym"@neorg.headings.2.title" { Title },
    sym"@neorg.headings.3.title" { Title, gui = "italic" },

    sym"@comment.warning.gitcommit" { DiagnosticError, gui = "reverse" },
  }
end)

-- return our parsed theme for extension or use else where.
return theme

-- vi:nowrap:ts=2:
