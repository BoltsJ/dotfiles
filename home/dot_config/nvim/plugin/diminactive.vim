if exists('g:loaded_diminactive')
  finish
endif
let g:loaded_diminactive=1

augroup diminactive
  autocmd!
  autocmd WinEnter,BufEnter * call dim#enter()
  " No BufLeave b/c https://github.com/neovim/neovim/issues/15300
  autocmd WinLeave * call dim#leave()
augroup END
