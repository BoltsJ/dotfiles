function! dim#enter() abort
  if exists('b:old_syntax')
    exe 'set syntax=' . b:old_syntax
    lua pcall(vim.treesitter.start)
  endif
endfunction

function! dim#leave() abort
  if !exists('b:nodim') && &syntax != 'OFF'
    lua vim.treesitter.stop()
    let b:old_syntax = &syntax
    set syntax=OFF
  endif
endfunction
