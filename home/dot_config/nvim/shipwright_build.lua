---@diagnostic disable: undefined-global
local lushwright = require("shipwright.transform.lush")
run(
  require("lush_theme.off_dark"),
  lushwright.to_lua,
  { patchwrite, vim.fn.stdpath("config") .. "/colors/off.lua", "-- DARK_OPEN", "-- DARK_CLOSE" }
)
run(
  require("lush_theme.off_light"),
  lushwright.to_lua,
  { patchwrite, vim.fn.stdpath("config") .. "/colors/off.lua", "-- LIGHT_OPEN", "-- LIGHT_CLOSE" }
)
