scriptencoding utf-8

let $PATH = $XDG_DATA_HOME . '/mise/shims:' . $PATH

lua <<EOF
local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup("plugins")
EOF

set tabstop=4
set shiftwidth=0
set softtabstop=-1
set expandtab
set mouse=nv
set mousemodel=extend
set autowrite
set title
set list
set listchars=tab:│\ ,extends:»,precedes:«,nbsp:·,trail:·

if $COLORTERM ==# 'truecolor'
  set termguicolors
  colorscheme off
else
  colorscheme quiet
endif

set statusline=%!v:lua.require('util.statusline').status()

set completeopt=menuone,noselect
inoremap <expr> <Return> pumvisible() ? "<C-Y>" : "<Return>"

nnoremap <F5> :silent! make! <bar> redraw!<CR>

augroup vimrc
autocmd!
if has('nvim')
  autocmd TermOpen * set bufhidden=hide
endif
autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif
augroup END

command! PackEdit exe "tabedit " . stdpath('config') . '/lua/plugins/init.lua'
command! LspEditServers exe "tabedit " . stdpath('config') . '/lua/config/lsp/servers.lua'
