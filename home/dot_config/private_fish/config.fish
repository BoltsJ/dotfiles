if status is-interactive
    atuin init fish | source
    mise activate fish | source
    # Commands to run in interactive sessions can go here
end
