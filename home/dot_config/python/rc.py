import atexit
import os
import readline

try:
    xdg_cache_home = os.path.join(os.environ['XDG_CACHE_HOME'])
except KeyError:
    xdg_cache_home = os.path.join(os.environ['HOME'], ".cache")
histfile = os.path.join(xdg_cache_home, "python_history")

try:
    readline.read_history_file(histfile)
    readline.set_history_length(1000)
except FileNotFoundError:
    pass

atexit.register(readline.write_history_file, histfile)
