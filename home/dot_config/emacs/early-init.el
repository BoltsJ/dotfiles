;;; early-init.el --- Early init file -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-startup-screen t)

(setq package-enable-at-startup nil)
;;; early-init.el ends here
