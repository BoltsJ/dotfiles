;;; eventide.el --- Follow system colorscheme on modern desktops  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(require 'dbus)
(defvar eventide--dbus-object nil)

(defgroup eventide nil
  "Settings for eventide.el."
  :group 'convenience)

(defcustom
  light-dark-color-themes
  '(modus-operandi . modus-vivendi)
  "Cons defining the light and then dark theme."
  :group 'eventide
  :type '(cons (symbol :tag "Light") (symbol :tag "Dark")))

(defcustom disable-prev-theme-on-switch
  t
  "Whether to call `disable-theme' on the opposite mode theme when switching."
  :group 'eventide
  :type 'boolean)

(defun eventide--set-theme (value)
  "VALUE is a number.  0 -> no pref; 1 -> prefer-dark; 2 -> prefer-light."
  (let ((disable-prev-theme-on-switch
         (and disable-prev-theme-on-switch
              (not (equal (car light-dark-color-themes)
                          (cdr light-dark-color-themes))))))
    (pcase value
      ((or 0 2)
       (load-theme (car light-dark-color-themes) t)
       (when disable-prev-theme-on-switch
         (disable-theme (cdr light-dark-color-themes))))
      (1
       (load-theme (cdr light-dark-color-themes) t)
       (when disable-prev-theme-on-switch
         (disable-theme (car light-dark-color-themes))))
      (_ (message "Invalid theme value")))))

(define-minor-mode eventide-mode
  "Toggle Eventide Mode.

When Eventide Mode is enabled, Emacs will change color themes when the
system changes color scheme."
  :global t
  :init-value nil
  :group 'eventide
  :lighter " eventide"
  :require 'dbus
  (when (and eventide-mode
             (not eventide--dbus-object))
    (dbus-call-method-asynchronously
     :session
     "org.freedesktop.portal.Desktop"
     "/org/freedesktop/portal/desktop"
     "org.freedesktop.portal.Settings"
     "Read"
     #'(lambda (val) (eventide--set-theme (car (car val))))
     "org.freedesktop.appearance"
     "color-scheme")
    (setq-default eventide--dbus-object
                  (dbus-register-signal
                   :session
                   "org.freedesktop.portal.Desktop"
                   "/org/freedesktop/portal/desktop"
                   "org.freedesktop.portal.Settings"
                   "SettingChanged"
                   #'(lambda (ns k val)
                       (when (and (string= ns "org.freedesktop.appearance")
                                  (string= k "color-scheme"))
                         (eventide--set-theme (car val)))))))
  (unless (or eventide-mode
              (not eventide--dbus-object))
    (dbus-unregister-object eventide--dbus-object)
    (setq-default eventide--dbus-object nil)))

(provide 'eventide)
;;; eventide.el ends here
