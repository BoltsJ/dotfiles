;;; init.el --- Init file  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:
;; XDG Directories
(defconst emacs-data-dir
  (expand-file-name "emacs/" (or (getenv "XDG_DATA_HOME") "~/.local/share")))
(defconst emacs-state-dir
  (expand-file-name "emacs/" (or (getenv "XDG_STATE_HOME") "~/.local/state")))
(defconst emacs-cache-dir
  (expand-file-name "emacs/" (or (getenv "XDG_CACHE_HOME") "~/.cache")))

(setq straight-vc-git-default-clone-depth 1)
(setq straight-base-dir emacs-data-dir)
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" straight-base-dir))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(straight-use-package 'use-package)
(eval-when-compile (require 'use-package))
(require 'bind-key)

(setq-default indent-tabs-mode nil
              tab-width 4
              make-backup-files nil
              vc-follow-symlinks t
              auto-save-list-file-prefix (concat
                                          emacs-cache-dir
                                          "/auto-save-list/.saves-"))
(add-to-list 'display-buffer-alist
             `(,(rx bos "*Flymake diagnostics for `" (* anychar) "'*" eos)
               (display-buffer-reuse-window
                display-buffer-in-side-window)
               (side . bottom)
               (reusable-frames . visible)
               (window-height . shrink-window-if-larger-than-buffer)))

;; Appearance stuff
(column-number-mode t)
;(add-to-list 'default-frame-alist '(font . "Comic Code-10"))
(set-face-attribute 'default nil :family "Comic Code" :height 100)
(set-face-attribute 'fixed-pitch nil :family "Comic Code")
(set-face-attribute 'variable-pitch nil :family "Noto Sans")

(use-package poet-theme
  :straight t)

(use-package eventide
  :load-path "lisp"
  :after poet-theme
  :init
  (setq light-dark-color-themes '(poet-monochrome . poet-dark-monochrome))
  (setq disable-prev-theme-on-switch t)
  :config (eventide-mode 1))

(use-package visual-fill-column
  :straight t
  :hook visual-line-mode
  :init (setq-default visual-fill-column-center-text t)
  :config (visual-fill-column-mode))

(use-package nerd-icons
  :straight t
  :defer t)
(use-package doom-modeline
  :straight t
  :defer 0
  :init (setq doom-modeline-modal nil)
  :config (doom-modeline-mode 1))

(use-package minions
  :straight t
  :after doom-modeline
  :init (setq doom-modeline-minor-modes t)
  :config (minions-mode 1))

(use-package highlight-indent-guides
  :straight t
  :hook (prog-mode . highlight-indent-guides-mode)
  :init
  (setq highlight-indent-guides-method 'character
        highlight-indent-guides-responsive 'top))

(use-package nyan-mode
  :straight t
  :commands (nyan-mode)
  :init
  (setq nyan-animate-nyancat t
        nyan-wavy-trail t))

;; Sane keybinds
(use-package undo-tree
  :straight t
  :init
  (setq undo-tree-history-directory-alist
        `(("." . ,(expand-file-name "undo/" emacs-cache-dir)))
        evil-undo-system 'undo-tree)
  :config (global-undo-tree-mode 1))
(use-package evil
  :straight t
  :after undo-tree
  :init
  (setq evil-want-C-u-scroll t)
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1)
  (evil-define-command evil-netrw-explore (arg)
    "Explore current directory or specified dir"
    (interactive "<f>")
    (dired arg))
  (evil-ex-define-cmd "E[xplore]" 'evil-netrw-explore))
(use-package evil-collection
  :straight t
  :after evil
  :config (evil-collection-init))

;; Utility
(use-package magit
  :straight t
  :hook (git-commit-setup . git-commit-turn-on-flyspell)
  :bind (("C-x g" . magit-status))
  :init
  (setq transient-history-file (expand-file-name "transient/history.el" emacs-cache-dir)
        transient-levels-file (expand-file-name "transient/levels.el" emacs-cache-dir)
        transient-values-file (expand-file-name "transient/values.el" emacs-cache-dir))
  (add-to-list 'display-buffer-alist
             `(,(rx bos "magit: " (* anychar) eos)
               (display-buffer-reuse-window
                display-buffer-in-side-window)
               (side . top)
               (reusable-frames . visible)
               (window-height . 0.30))))

(use-package lsp-mode
  :straight t
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-session-file (expand-file-name "lsp-session-v1" emacs-state-dir)))
(use-package lsp-ui :straight t :disabled :commands lsp-ui-mode)
(use-package flycheck
  :straight t
  :commands (flycheck-mode flycheck-global-mode)
  :init
  (add-to-list 'display-buffer-alist
             `(,(rx bos "*Flycheck errors*" eos)
               (display-buffer-reuse-window
                display-buffer-in-side-window)
               (side . bottom)
               (reusable-frames . visible)
               (window-height . shrink-window-if-larger-than-buffer))))

(use-package editorconfig :straight t :config (editorconfig-mode 1))
(use-package yasnippet-snippets :straight t :defer t)
(use-package yasnippet
  :straight t
  :hook (lsp-after-open . yas-minor-mode)
  :commands (yas-global-mode yas-minor-mode))

(use-package vterm
  :straight t
  :commands (vterm vterm-other-window)
  :init (setq vterm-kill-buffer-on-exit t))

(use-package corfu
  :straight t
  :hook (evil-insert-state-entry . global-corfu-mode)
  :bind (:map corfu-map
         ("S-SPC" . corfu-insert-separator)
         ("RET" . corfu-complete)
         :map evil-insert-state-map
         ("C-x C-o" . completion-at-point))
  :commands global-corfu-mode)

(use-package flyspell
  :defer t
  :init
  (setq flyspell-issue-message-flag nil
        ispell-dictionary "american"))

;; New file modes
(use-package js
  :defer t
  :hook (js-mode . lsp-deferred)
  :init
  (setq js-indent-level 2))
(use-package markdown-mode
  :straight t
  :mode "\\.md\\'")
(use-package typescript-mode
  :straight t
  :hook (typescript-mode . lsp-deferred)
  :mode "\\.ts\\'"
  :init
  (setq typescript-indent-level 2))
(use-package svelte-mode
  :straight t
  :hook (svelte-mode . lsp-deferred)
  :mode "\\.svelte\\'")
(use-package fish-mode
  :straight t
  :hook (fish-mode . (lambda ()
                       (add-hook 'before-save-hook 'fish_indent-before-save)))
  :mode "\\.fish\\'")

;; Builtins
(use-package tramp
  :defer t
  :init
  (setq tramp-auto-save-directory
        (expand-file-name "tramp/auto-save/" emacs-cache-dir))
  (setq tramp-persistency-file-name
        (expand-file-name "tramp/persistency.el" emacs-cache-dir)))
;;; init.el ends here
