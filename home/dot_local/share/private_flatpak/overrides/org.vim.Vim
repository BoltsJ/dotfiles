[Environment]
VIMINIT=set nocp | source $XDG_CONFIG_HOME/vim/vimrc
NPM_CONFIG_USERCONFIG=
GVIMINIT=source $XDG_CONFIG_HOME/vim/gvimrc

[Context]
unset-environment=NPM_CONFIG_USERCONFIG;
