#!/bin/bash

toolbox create
toolbox run sudo dnf install -y $(cat ~/.local/share/chezmoi/toolbox-packages.txt)
toolbox run sudo install ~/.local/share/chezmoi/toolbox-configs/etc/profile.d/zz-toolbox-env.sh /etc/profile.d/
